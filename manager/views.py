from django.shortcuts import render
from django.http import HttpResponse
from django.core import serializers
from manager.models import  Site

# Create your views here.

def createJson(request):

    data = serializers.serialize('json',Site.objects.all())
    return HttpResponse(data)
