from django.db import models

# Create your models here.

class Site(models.Model):
    url = models.URLField()
    duration = models.IntegerField()
    description = models.TextField()

    def __unicode__(self):
        return self.description