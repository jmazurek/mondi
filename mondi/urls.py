from django.conf.urls import patterns, include, url

from django.contrib import admin
import manager.views

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mondi.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^json/',manager.views.createJson)
)
